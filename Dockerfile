ARG BASE_IMAGE
ARG BUILD_IMAGE

# compile NED
FROM $BUILD_IMAGE AS build
ARG PACKAGE_NAME

COPY ${PACKAGE_NAME} /var/opt/ncs/packages/${PACKAGE_NAME}
# default shell is ["/bin/sh", "-c"]. We add -l so we get a login shell which
# means the shell reads /etc/profile on startup. /etc/profile includes the files
# in /etc/profile.d where we have ncs.sh that sets the right paths so we can
# access ncsc and other NSO related tools.
SHELL ["/bin/sh", "-lc"]
RUN make -C /var/opt/ncs/packages/${PACKAGE_NAME}/netsim
RUN make -C /var/opt/ncs/packages/${PACKAGE_NAME}/src

# build a netsim docker image by copying the compiled NED and changing the
# default CMD to one that runs netsim, thus running this docker image will per
# default startup a netsim instance
FROM $BUILD_IMAGE as netsim
ARG PACKAGE_NAME
# slight hack to set the *run time* environment variable PNAME to the
# *build-time* argument PACKAGE_NAME, this is so this Dockerfile can remain
# generic, yet write static information (the package name) to the resulting
# Docker image. Directly using PACKAGE_NAME in the CMD line below would mean
# that it requires the PACKAGE_NAME variable to be set at run time.
ENV PNAME=${PACKAGE_NAME}

COPY --from=build /var/opt/ncs/packages /var/opt/ncs/packages

# default shell is ["/bin/sh", "-c"]. We add -l so we get a login shell which
# means the shell reads /etc/profile on startup. /etc/profile includes the files
# in /etc/profile.d where we have ncs.sh that sets the right paths so we can
# access ncsc and other NSO related tools. This is required for netsim to start.
SHELL ["/bin/sh", "-lc"]

COPY /run-netsim.sh /run-netsim.sh

CMD /run-netsim.sh ${PNAME} ${PNAME}

# produce an NSO image that comes loaded with our NED - perfect for our testing,
# but probably not anything beyond that since you typically want more NSO
# packages for a production environment
FROM $BASE_IMAGE AS nso

COPY --from=build /var/opt/ncs/packages /var/opt/ncs/packages

# build a minimal image that only contains the NED itself - perfect way to
# distribute the compiled NED by relying on Docker package registry
# infrastructure
FROM scratch AS ned

COPY --from=build /var/opt/ncs/packages /var/opt/ncs/packages
